module gitlab.com/greut/eclint

go 1.17

require (
	github.com/editorconfig/editorconfig-core-go/v2 v2.4.3
	github.com/go-logr/logr v1.1.0
	github.com/gogs/chardet v0.0.0-20191104214054-4b6791f73a28
	github.com/google/go-cmp v0.5.6
	github.com/karrick/godirwalk v1.16.1
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/mattn/go-colorable v0.1.9
	golang.org/x/term v0.0.0-20210916214954-140adaaadfaf
	k8s.io/klog/v2 v2.20.0
)

require (
	github.com/mattn/go-isatty v0.0.12 // indirect
	golang.org/x/mod v0.5.0 // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
	gopkg.in/ini.v1 v1.63.2 // indirect
)
